// словарь для vee-validate
/* eslint-disable */
const messages = {
    _default: field => `Не верный формат`,
    after: (field, [target]) => `${field} harus sebelum ${target}.`,
    alpha_dash: field =>
        `${field} boleh mengandung karakter alfanumerik, tanda hubung, dan garis bawah.`,
    alpha_num: field => `${field} hanya boleh mengandung karakter alfanumerik.`,
    alpha_spaces: field =>
        `${field} hanya boleh berisi karakter alfabet serta spasi.`,
    alpha: 'Разрешены только буквы',
    before: (field, [target]) => `${field} harus setelah ${target}.`,
    between: (field, [min, max]) =>
        `${field} harus di antara ${min} dan ${max}.`,
    confirmed: (field, [confirmedField]) =>
        `${field} tidak cocok dengan ${confirmedField}.`,
    credit_card: field => `${field} tidak sah.`,
    date_between: (field, [min, max]) =>
        `${field} harus di antara ${min} dan ${max}.`,
    date_format: (field, [format]) => 'Не верный формат даты',
    decimal: (field, [decimals = '*'] = []) =>
        `${field} harus berupa angka dan boleh mengandung ${
            decimals === '*' ? '' : decimals
        } titik desimal.`,
    digits: (field, [length]) => `${field} harus berupa ${length} digit angka.`,
    dimensions: (field, [width, height]) =>
        `${field} harus berdimensi lebar ${width} pixel dan tinggi ${height} pixel.`,
    email: field => `Некорректный формат`,
    ext: field => `${field} harus berupa berkas yang benar.`,
    image: field => `${field} harus berupa gambar.`,
    in: field => `${field} harus berupa nilai yang sah.`,
    integer: field => `${field} harus berupa bilangan bulat.`,
    ip: field => `${field} harus berupa alamat ip yang sah.`,
    length: (field, [length, max]) => {
        if (max) {
            return `Panjang ${field} harus di antara ${length} dan ${max}.`;
        }
        return `Длина должна быть ${length} символа(ов)`;
    },
    max: (field, [length]) =>
        `Длина должна быть не более ${length} символа(ов)`,
    max_value: (field, [size]) =>
        `Nilai ${field} tidak boleh lebih dari ${size}.`,
    mimes: field => `Tipe berkas ${field} harus benar.`,
    min: (field, [length]) => `${field} minimal mengandung ${length} karakter.`,
    min_value: (field, [size]) =>
        `Nilai ${field} tidak boleh kurang dari ${size}.`,
    not_in: field => `${field} harus berupa nilai yang sah.`,
    numeric: 'Допустимы только цифры',
    regex: field => `Некорректный формат`,
    required: 'Заполните поле',
    url: field => `${field} harus berupa tautan yang benar.`
};

const attributes = {
    // firstName: 'имя',
    // lastName: 'фамилию',
    // patronymic: 'отчество',
    // phone: 'телефон',
    // code: 'код верификации',
    // date: 'дата рождения'
};

const locale = {
    name: 'ru',
    messages,
    attributes
};

export default locale;

// module.exports = {
//   filenameHashing: false,
//   lintOnSave: false,
//   publicPath: '/',
//   outputDir: undefined,
//   assetsDir: 'src/assets/',
//   runtimeCompiler: undefined,
//   productionSourceMap: undefined,
//   parallel: undefined,
//
//   css: {
//     loaderOptions: {
//       sass: {
//         data: `
//           @import "@/assets/scss/main.scss";
//         `
//       }
//     }
//   },
//
//   pluginOptions: {
//     moment: {
//       locales: [
//         ''
//       ]
//     }
//   }
// }


const path = require('path');

module.exports = {
  pluginOptions: {
    // moment: {
    //     locales: ['ru']
    // }
  },

  configureWebpack: {
    resolve: {
      alias: {
        '@view': path.join(__dirname, 'src/views'),
        '@component': path.join(__dirname, 'src/components'),
        '@store': path.join(__dirname, 'src/store'),
        '@styles': path.join(__dirname, 'src/assets/scss'),
        '@utils': path.join(__dirname, 'src/utils'),

      },
      extensions: ['.vue', '.js', '.json', 'scss']
    },

    output: {
      filename: '[name].js',
      chunkFilename: '[name].js'
    }
  },
  css: {
    extract: {
      filename: 'css/[name].css',
      chunkFilename: 'css/[name].css',
    }
  },

  publicPath: process.env.NODE_ENV === 'production' ? '/' : '',


};

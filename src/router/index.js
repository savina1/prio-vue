import Vue from 'vue'
import VueRouter from 'vue-router'
import consumerCredit from '@view/consumerCredit.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'consumerCredit',
    component: consumerCredit
  }
]

const router = new VueRouter({
  mode: 'abstract',
  base: process.env.BASE_URL,
  routes
})

export default router

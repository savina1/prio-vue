import Vue from 'vue'
// import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'

import { ValidationProvider, ValidationObserver, extend, localize } from 'vee-validate';
import { required } from 'vee-validate/dist/rules';
import * as rules from "vee-validate/dist/rules";
import ru from "vee-validate/dist/locale/ru.json";

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});

localize("ru", ru);

localize({ru: {
    names: {
      password: 'пароль',
      email: 'email',
      tel: 'телефон'
    }
  }});


extend('required', {
  ...required,
  message:'Обязательное поле'
});

import { setInteractionMode } from 'vee-validate';

setInteractionMode('lazy');


import RangeSlider from 'vue-range-slider'
import 'vue-range-slider/dist/vue-range-slider.css';

import vSelect from 'vue-select';
import 'vue-select/dist/vue-select.css';
Vue.component('vSelect', vSelect);

import AppInput from '@/components/Input.vue';
Vue.component('AppInput', AppInput);


import PriceInput from '@/components/PriceInput.vue';
Vue.component('PriceInput', PriceInput);


import PopupContent from '@/components/PopupContent.vue';
Vue.component('PopupContent', PopupContent);

import PopupRow from '@/components/PopupRow.vue';
Vue.component('PopupRow', PopupRow);

import Popup from '@/decorators/Popup.vue';
Vue.component('Popup', Popup);

import AnimatedNumber from '@/components/AnimatedNumber.vue';
Vue.component('RangeSlider', RangeSlider);

import VScrollLock from 'v-scroll-lock'
Vue.use(VScrollLock)

Vue.filter(
    'priceFormat',
    (value, currency) => {
      if (value) {
        return value.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")
      }

    }
);

Vue.filter(
    'priceCalcFormat',
    (value, currency) => {
      if (value) {
        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")
      }

    }

);

import 'normalize-scss'
import "@styles/main.scss"
Vue.config.productionTip = false

extend('required', {
  ...required,
  message:'обязательное поле'
});

Vue.component('ValidationProvider', ValidationProvider);

Vue.component('animated-integer', AnimatedNumber)


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

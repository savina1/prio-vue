import Vue from 'vue'
import Vuex from 'vuex'
import { getField, updateField } from 'vuex-map-fields'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    debt: 0,
    calcTable:[],
    popupScheduleActive: false,
    radioCreditTypes: [{
      value: 'Anuetent',
      text: 'Ануетентный платеж'
    },
      {
        value: 'Differenc',
        text: 'Дифференцированный платеж'
      }
    ],
    checkedCreditType: 'Anuetent',
    calculatedTotalCalcPercents: '',



    creditTypeOptions: ['Потребительские цели', 'Развитие бизнеса', 'Автокредит', 'Ипотека','Рефинансирование'],
    mail:'',
    fio:'',
    number:'',
    amount:'' ,
    creditTerm:'',
    priceIsShown: true,
    creditType:'Потребительский кредит',
    checkboxChecked: true,
    checkedSalaryInPrio: false,
    checkedIpoteka: true,
    checkedGosuslugi: true,
    checkedCreditInPrio: false,
    consumerRate: '',
    firstNumber: 20,
    secondNumber: 40,
    schema:{
      mail:{
        name:'mail',
        mask:'',
        label: 'Ваш E-mail',
        validateRules:{
          required: true,
          regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        }
      },
      fio:{
        name:'fio',
        label: 'Фамилия, имя и отчество<sup>*</sup>',
        mask:'',
        validateRules:{
          required: true
        }
      },
      number:{
        placeholder:'+ 7 (980) 693 79 09',
        name:'number',
        label: 'Ваш номер телефона<sup>*</sup>',
        mask: '+7 (999) 999 99 99',
        validateRules:{
          required: true,
          regex: /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/
        }
      },
      amount: {
        placeholder: '2 000 000',
        name: 'amount',
        label: 'Сумма кредита',
        mask: '[0-9 ]{0,10}',
        maskRegex: true,
        validateRules: {
          required: true
        }
      },
      creditTerm: {
        placeholder:'1 год',
        name:'creditTerm',
        label: 'Срок кредита',
      },
    }
  },
  getters: {
    getField
  },
  mutations: {
    updateField,
    setData(state, payload) {
      const {amount, consumerRate, creditTerm} = payload
      state.amount = amount;
      state.consumerRate = consumerRate;
      state.creditTerm = creditTerm;
    }
  },
  actions: {
    async getData({getters, commit}) {
      const data = {
        amount:100000,
        consumerRate:9.9,
        creditTerm:12
      }
      commit('setData', data)
    }
  },
  modules: {
  }
})
